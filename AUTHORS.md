## Main Authors:


Author                               | Contact
------------------------------------ | ------------------------------------
Marcos Rodrigues Vieira¹ ᵉ ²         | <marcos.vieira@hal.hitachi.com>
Pedro Sena Tanaka³                   | <pedro.stanaka@gmail.com>
Denis Evangelista Sanches³           | <sanches.e.denis@gmail.com>
Daniel dos Santos Kaster³            | <dskaster@uel.br>


## Affiliation:


1. University of California - Riverside, USA
2. Big Data Lab – Hitachi America, Ltd. – R&D, USA
3. Department of Computer Science – University of Londrina, Brazil
